import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

import { setupDB } from './db';

export const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.disable('x-powered-by');

export async function bootstrap() {
  const db = await setupDB();
  const projectsRouter = express.Router();

  const handler = (req, res, next) => {
    res.json({ message: 'hello world' });
  };

  app.get('/', handler);

  projectsRouter.post('/', async (req, res) => {
    const { name } = req.body;

    const slug = name.toLowerCase();

    const project = {
      slug,
      boards: [],
    };

    const existingProject = await db.collection('projects').findOne({ slug });

    if (existingProject) {
      res.status(400).json({ error: `project ${slug} already exists` });
    } else {
      await db.collection('projects').insertOne(project);
      res.json({ project });
    }
  });

  const findProject = async (req, res, next) => {
    const { slug } = req.params;

    const project = await db.collection('projects').findOne({ slug });

    if (!project) {
      res.status(400).json({ error: `project ${slug} does not exist` });
    } else {
      req.body.project = project;
      next();
    }
  };

  const findBoard = async (req, res, next) => {
    const { name } = req.params;
    const { project } = req.body;

    const board = project.boards.find(b => b.name === name);

    if (!board) {
      res.status(400).json({ error: `board ${name} does not exist inside project ${project.slug}` });
    } else {
      req.body.board = board;
      next();
    }
  }

  projectsRouter.get('/:slug', findProject, async (req, res) => {
    const { project } = req.body;
    res.json({ project });
  });

  projectsRouter.post('/:slug/boards', findProject, async (req, res) => {
    const { name, project } = req.body;
    const board = { name, tasks: [] };
    project.boards.push(board);

    await db
      .collection('projects')
      .findOneAndReplace({ slug: project.slug }, project);

    res.json({ board });
  });

  projectsRouter.delete('/:slug', findProject, async (req, res) => {
    const { project } = req.body;

    await db
      .collection('projects')
      .deleteOne(project);

    res.json({ project });
  });

  projectsRouter.delete('/:slug/boards/:name', findProject, findBoard, async (req, res) => {
    const { project, board } = req.body;

    project.boards = project.boards.filter(b => b.name !== board.name);

    await db
      .collection('projects')
      .findOneAndReplace({ slug: project.slug }, project);

    res.json({ project });
  });

  projectsRouter.post('/:slug/boards/:name/tasks', findProject, findBoard, async (req, res) => {
    const { project, board, task } = req.body;

    project
      .boards.find(b => b.name === board.name)
      .tasks.push(task);

    await db
      .collection('projects')
      .findOneAndReplace({ slug: project.slug }, project);

    res.json({ project });
  });

  projectsRouter.delete('/:slug/boards/:name/tasks/:id', findProject, findBoard, async (req, res) => {
    const { id } = req.params;
    const { project, board } = req.body;

    const boardRef = project.boards.find(b => b.name === board.name)
    const task = boardRef.tasks.find(t => t.id === id);

    if (!task) {
      res.status(400).json({ error: `task ${id} does not exist inside board ${board.name}` });
    } else {
      boardRef.tasks = boardRef.tasks.filter(t => t.id !== id)

      await db
        .collection('projects')
        .findOneAndReplace({ slug: project.slug }, project);

      res.json({ project });
    }
  });

  app.use('/projects', projectsRouter);
  return db;
}
