import request from 'supertest';

import { app, bootstrap } from '../src/app';

describe('our app', () => {
  let db;

  beforeAll(async () => (db = await bootstrap()));

  beforeEach(async () => {
    await db.dropDatabase(process.env.DB_NAME + '_test');
  });

  afterAll(async () => await db.close());

  // código pt. 1
  test('GET / returns a "hello world"', async () => {
    const { body } = await request(app).get('/');

    expect(body.message).toBeDefined();
    expect(body.message).toBe('hello world');
  });

  // código pt. 2
  test('POST /projects returns a new project slug', async () => {
    const { body } = await request(app)
      .post('/projects')
      .send({ name: 'Awesome' });

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('awesome');
  });

  // código pt.2
  test('GET /projects/:slug returns a matching project', async () => {
    await request(app).post('/projects').send({ name: 'Awesome' });

    const { body } = await request(app).get('/projects/awesome');

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('awesome');
  });

  // código pt. 3
  test('POST /projects/:slug/boards adds a new board into a project', async () => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'todo';

    const { body } = await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });

    expect(body.board).toBeDefined();
    expect(body.board.name).toBe(name);
  });

  test('DELETE /:slug call to non-existent project returns error', async () => {
    const slug = 'non-existent slug';
    const { statusCode, body } = await request(app)
      .delete(`/projects/${slug}`);

    expect(statusCode).toBe(400);
    expect(body.error).toBeDefined();
    expect(body.error).toBe('project non-existent slug does not exist');
  });

  test('DELETE /:slug deletes project and returns it', async () => {
    const project = { name: 'deleted project' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const { body } = await request(app)
      .delete(`/projects/${slug}`);

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('deleted project');
  });

  test('DELETE /:slug/boards/:name call to non-existent project returns error', async () => {
    const slug = 'non-existent slug';
    const name = 'any name';
    const { statusCode, body } = await request(app)
      .delete(`/projects/${slug}/boards/${name}`);

    expect(statusCode).toBe(400);
    expect(body.error).toBeDefined();
    expect(body.error).toBe('project non-existent slug does not exist');
  });

  test('DELETE /:slug/boards/:name call to non-existent board returns error', async () => {
    const project = { name: 'existent project' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'non-existent board name';
    const { statusCode, body } = await request(app)
      .delete(`/projects/${slug}/boards/${name}`);

      expect(statusCode).toBe(400);
      expect(body.error).toBeDefined();
      expect(body.error).toBe('board non-existent board name does not exist inside project existent project');
  });

  test('DELETE /:slug/boards/:name delete board and returns updated project', async () => {
    const project = { name: 'existent project' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'existent board';
    await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });
    
    const { body } = await request(app)
      .delete(`/projects/${slug}/boards/${name}`);

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('existent project');
    expect(body.project.boards.find(b => b.name === name)).toBeUndefined();
  });

  test('POST /:slug/boards/:name/tasks call to non-existent project returns error', async () => {
    const slug = 'non-existent slug';
    const name = 'any name';
    const { statusCode, body } = await request(app)
      .post(`/projects/${slug}/boards/${name}/tasks`);

    expect(statusCode).toBe(400);
    expect(body.error).toBeDefined();
    expect(body.error).toBe('project non-existent slug does not exist');
  });

  test('POST /:slug/boards/:name/tasks call to non-existent board returns error', async () => {
    const project = { name: 'existent project' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'non-existent board name';
    const { statusCode, body } = await request(app)
      .post(`/projects/${slug}/boards/${name}/tasks`);

      expect(statusCode).toBe(400);
      expect(body.error).toBeDefined();
      expect(body.error).toBe('board non-existent board name does not exist inside project existent project');
  });

  test('POST /:slug/boards/:name/tasks creates task and returns it', async () => {
    const project = { name: 'existent project' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'existent board';
    await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });

    const task = { id: 'any id', priority: 'any priority', title: 'any title', description: 'any description' }
    const { body } = await request(app)
      .post(`/projects/${slug}/boards/${name}/tasks`)
      .send({ task });

    const resultProject = body.project;
    const resultBoard = resultProject.boards.find(b => b.name === name);
    const resultTask = resultBoard.tasks.find(t => t.id === task.id)

    expect(resultProject).toBeDefined();
    expect(resultProject.slug).toBe('existent project');
    expect(resultBoard).toBeDefined();
    expect(resultTask).toBeDefined();
  });

  test('DELETE /:slug/boards/:name/tasks/:id call to non-existent project returns error', async () => {
    const slug = 'non-existent slug';
    const name = 'any name';
    const id = 'any id';
    const { statusCode, body } = await request(app)
      .delete(`/projects/${slug}/boards/${name}/tasks/${id}`);

    expect(statusCode).toBe(400);
    expect(body.error).toBeDefined();
    expect(body.error).toBe('project non-existent slug does not exist');
  });

  test('DELETE /:slug/boards/:name/tasks/:id call to non-existent board returns error', async () => {
    const project = { name: 'existent project' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'non-existent board name';
    const id = 'any id';
    const { statusCode, body } = await request(app)
      .delete(`/projects/${slug}/boards/${name}/tasks/${id}`);

    expect(statusCode).toBe(400);
    expect(body.error).toBeDefined();
    expect(body.error).toBe('board non-existent board name does not exist inside project existent project');
  });

  test('DELETE /:slug/boards/:name/tasks/:id call to non-existent task returns error', async () => {
    const project = { name: 'existent project' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'existent board';
    await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });

    const id = 'non-existent task id'
    const { statusCode, body } = await request(app)
      .delete(`/projects/${slug}/boards/${name}/tasks/${id}`);

    expect(statusCode).toBe(400);
    expect(body.error).toBeDefined();
    expect(body.error).toBe('task non-existent task id does not exist inside board existent board');
  });

  test('DELETE /:slug/boards/:name/tasks/:id successfully deletes task and returns updated project', async () => {
    const project = { name: 'existent project' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'existent board';
    await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });

    const task = { id: 'any id', priority: 'any priority', title: 'any title', description: 'any description' }
    await request(app)
      .post(`/projects/${slug}/boards/${name}/tasks`)
      .send({ task });

    const { body } = await request(app)
      .delete(`/projects/${slug}/boards/${name}/tasks/${task.id}`);

    const resultProject = body.project;
    const resultBoard = resultProject.boards.find(b => b.name === name);
    const resultTask = resultBoard.tasks.find(t => t.id === task.id);

    expect(resultProject).toBeDefined();
    expect(resultProject.slug).toBe('existent project');
    expect(resultBoard).toBeDefined();
    expect(resultTask).toBeUndefined();
  });
});
